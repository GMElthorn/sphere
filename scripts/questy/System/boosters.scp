/////////////////////////////////////
//                                 //
//         Player Boosters         //
//                                 //
/////////////////////////////////////

// Rewards player with a new booster
// argv0: booster number gathered as an order number of a booster from defnames
[function playerBooster_add]
arg(BoosterMemory,<findId(i_playerBoosters)>)
if !(<hval arg(BoosterMemory)>) // the player hasn't got any boosters yet
  newequip(i_playerBoosters)
  arg(BoosterMemory,<lastnew>)
endif
f_playerBooster_storeBoostLink(<arg(BoosterMemory)>,<argv(0)>)

// Triggers a stored booster.
// Called upon the player who has the booster.
// Only ONE booster can run at a time.
// argv(0) - the booster number gathered from the booster ITEM
//         - the order may vary for each player based on the order in which the boosters were acquired
// return 0 - if the booster is applied successfully
//        1 - if the player already has a running boost
[function playerBooster_use]
if (findId(i_booster))
 return 1
endif
arg(BoosterMemory,<findId(i_playerBoosters)>)
arg(boostId,<eval BoosterMemory.tag(boosterId[<eval argv(0)>])>)
newItemSafe(i_booster)
lastnew.tag(boostId,<arg(boostId)>)
lastnew.tag(boostType,<def_playerBooster_type[<arg(boostId)>]>)
lastnew.more1=<def_playerBooster_value[<arg(boostId)>]>
lastnew.equip
lastnew.timer=<eval <def_playerBooster_duration[<arg(boostId)>]>*3600> // duration in hours
f_playerBooster_decreaseBoostLink(<arg(BoosterMemory)>,<argv(0)>)
accmsg("pouzil Booster <def_playerBooster_description[<arg(boostId)>]> na pozici <p> (<region.name>)")

/////////////////////
// B O O S T E R S //
/////////////////////
[defnames def_playerBoosters]
def_playerBooster_typesNumber         1
def_playerBooster_type_XP             1

def_playerBooster_description[0]      "+25% zkusenosti na 1h"
def_playerBooster_icon[0]             03300
def_playerBooster_type[0]             def_playerBooster_type_XP
def_playerBooster_value[0]            25
def_playerBooster_duration[0]         1 //hours

def_playerBooster_description[1]      "+25% zkusenosti na 2h"
def_playerBooster_icon[1]             03300
def_playerBooster_type[1]             def_playerBooster_type_XP
def_playerBooster_value[1]            25
def_playerBooster_duration[1]         2 //hours

def_playerBooster_description[2]      "+25% zkusenosti na 4h"
def_playerBooster_icon[2]             03300
def_playerBooster_type[2]             def_playerBooster_type_XP
def_playerBooster_value[2]            25
def_playerBooster_duration[2]         4 // hours

def_playerBooster_description[3]      "+50% zkusenosti na 1h"
def_playerBooster_icon[3]             03300
def_playerBooster_type[3]             def_playerBooster_type_XP
def_playerBooster_value[3]            50
def_playerBooster_duration[3]         1 // hours

def_playerBooster_description[4]      "+50% zkusenosti na 2h"
def_playerBooster_icon[4]             03300
def_playerBooster_type[4]             def_playerBooster_type_XP
def_playerBooster_value[4]            50
def_playerBooster_duration[4]         2 // hours

def_playerBooster_description[5]      "+50% zkusenosti na 4h"
def_playerBooster_icon[5]             03300
def_playerBooster_type[5]             def_playerBooster_type_XP
def_playerBooster_value[5]            50
def_playerBooster_duration[5]         4 // hours

//////////////////
// C O M M O N  // 
//////////////////

// Serves as the booster TAG storage - i.e. carries all the unused boosters
// a player acquired in the game.
[itemdef i_playerBoosters]
ID=i_memory
TYPE=t_eq_script
name=Player Boosters

on=@timer
return 1

// An activated booster item. Represents a SINGLE booster activated.
[itemdef i_booster]
ID=i_skull_candle_lit
TYPE=t_eq_script
name=Player Boost

//tags
// :boostId:     - The boost ID gathered from the defnames
// :boostType:   - The type of the boost

on=@equip
if (tag(boostType) == def_playerBooster_type_XP)
  cont.f_booster_xpBoost_apply(<uid>)
endif

on=@unEquip
if (tag(boostType) == def_playerBooster_type_XP)
  cont.f_booster_xpBoost_remove
endif

on=@timer
remove

// argv(0) booster memory
// argv(1) booster ID gathered as an order number from defnames
[function f_playerBooster_storeBoostLink]
arg(i,0)
while (i < <eval argv(0).tag(boosterIdsCount)>)
  if (argv(0).tag(boosterId[<eval arg(i)>]) == argv(1))
    argv(0).tag(boosterAmount[<eval arg(i)>],#+1)
    return // already has the link
  endif
  arg(i,#+1)
endwhile

argv(0).f_customLink_add(<argv(1)>,boosterId) // add new link to the given booster ID.
argv(0).f_customLink_add(1,boosterAmount)   // link the number of the given boosters stored

// argv(0) booster memory
// argv(1) - the booster number gathered from the booster ITEM
//         - the order may vary for each player based on the order in which the boosters were acquired
[function f_playerBooster_decreaseBoostLink]
if (argv(0).tag(boosterAmount[<eval argv(1)>]) > 1)
  argv(0).tag(boosterAmount[<eval argv(1)>],#-1)
else
  argv(0).f_customLink_remove(boosterId,<argv(1)>)
  argv(0).f_customLink_remove(boosterAmount,<argv(1)>)
endif

///////////////////////////////////////////
// B O O S T E R   D E F I N I T I O N S //
///////////////////////////////////////////

///////////////
//    X P    //

[function f_booster_xpBoost_apply]   // argv(0) - UID of Booster memory item
events=+e_boosted_xp
tag(xpBooster_link,<argv(0)>) // in case we have more concurrent boosters in the future, it's handy to store the direct link to an XP booster type

[function f_booster_xpBoost_remove]
events=-e_boosted_xp
tag.remove(xpBooster_link)
classMessage("Tvuj bonus do zkusenosti vyprsel!")

[events e_boosted_xp]

[eof]
