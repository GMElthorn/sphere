These are macros and syntax highlight for UltraEdit (compatible with v12.00a).

Usage:
1. Run UltraEdit, navigate to Advanced->Configuration->Editor Display->Syntax Highlighting
2. Click Browse at the 'full path name for wordlist' text box
3. Navigate to the wordfile.txt in this directory and hit Open
4. Apply your new settings and enjoy. You may further change behavior by hitting the 'Set Colors' button.
5. (optional) include the macro file ueditQuestsMacros in Macro->Load + navigate to the macro file

To view the list of macros and their shortcuts, go to Macro->Edit Macro->Modify. There, you will see the list of macros and their default shortcuts
It's basically just a list of text inputs, that will place a whole line with default values for the most commonly used gump inputs. If trained
properly, this should largely reduce the time spent re-typing the gump framework functions and their arguments.
